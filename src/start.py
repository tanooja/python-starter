
# displaying numbers at even places
def even(list):
    res = []
    for i in range(len(list)):
        if i % 2 == 0:
            res.append(list[i])
            i = i + 1
    return res
print(even([1, 5, 84, 6, 43, 89]))


# max of an array
def max(arr):
    res = arr[0]
    for i in range(len(arr)):
        if arr[i] > res:
            res = arr[i]
        i = i + 1
    return res
print(max([4, 23, 56, 2, 6, 0]))


# zip function
def zip(coll1,coll2):
    res=[]
    if len(coll1)<len(coll2):
        l=len(coll1)
    else:
        l=len(coll2)
        for i in range(l):
            res.append([coll1[i],coll2[i]])
    return res
print(zip([2,5,8],[9,0,2]))


# copy elements into single array
def copy(list):
    res = []
    for i in range(len(list)):
        res.append(list[i])
        i = i + 1
    return res
print(copy([4, 7, 9, 3, 8]))


#mapping elements to its index
def listToMap(list):
    res = []
    for i in range(len(list)):
        res.append({i: list[i]})
        i = i + 1
    return res
print(listToMap([3, 8, 10, 45]))


# unique
def unique(list):
    res = []
    for i in list:
        if i not in res:
            res.append(i)

    return res
print(unique([2, 5, 2, 5, 3, 4, 6, 3, 4]))


# converting elements of an array into string
def conversion(list):
    res =""
    for i in list:
        res=res+str(i)
    return res
print(conversion([1, 2, 3, 4, 5]))


# index of an array
def index(arr,num):
    res=0
    for i in range(len(arr)):
        if num in arr:
            if num==arr[i]:
                res = i
        else:
            res=-1
    return res
print(index([3,6,9],9))


# reverse of a string
def reverse(string):
    res=""
    for i in string:
        res=i+res
    return res
print(reverse("tanuja"))


# check whether a given string is palindrome or not
def palindrome(string):
    res=""
    for i in string:
        res=i+res
    if res == string:
        return True
    else:
        return False
print(palindrome("tannat"))


# perfect
def perfect(num):
    factors=[]
    sum=0
    for i in range(1,num):
        if num%i==0:
            factors.append(i)
    for j in range(len(factors)):
                sum=sum+factors[j]
    if sum==num:
        return True
    else:
        return False
print(perfect(5))
print(perfect(6))


# def prime(num):
#     for i in range(2, num):
#         if num % i == 0:
#             return False
#     return True


# prime
def prime(num):
    factors=[]
    for i in range(1, num+1):
        if num % i == 0:
            factors.append(i)
    if len(factors) == 2:
        return True
    else:
        return False
print(prime(15))


# gcd
def gcd(num1,num2):
    res=[]
    factors1=[]
    factors2=[]
    for i in range(1, num1+1):
        if num1 % i == 0:
            factors1.append(i)
    for i in range(1, num2+1):
        if num2 % i == 0:
            factors2.append(i)
    for i in factors1:
        if i in factors2:
            res.append(i)
        i = i + 1
    return max(res)
print(gcd(6,9))
print(gcd(1,5))


# lcm of two
def lcm2(num1,num2):
    res=(num1*num2)//gcd(num1,num2)
    return res
print(lcm2(12,3))


# lcm of three
def lcm3(num1,num2,num3):
    res1=lcm2(lcm2(num1,num2),num3)
    return res1
print(lcm3(12,5,4))


# gcd of three
def gcd3(num1,num2,num3):
    res1=gcd(gcd(num1,num2),num3)
    return res1
print(gcd3(12,5,4))


# armstrong
def armstrong(num):
    res=0
    count=len(str(num))
    for i in str(num):
        res=res+(int(i)**count)
        i = int(i)+1
    if res==num:
        return True
    else:
        return False
print(armstrong(122))
print(armstrong(153))


# reverse of a number
def reverse(num):
    res=0
    a=[]
    n = num
    while (n > 0):
        a.append(n % 10)
        n = n // 10
    for i in range(len(a)):
        res=res+(a[i]*((10**(len(a)-(i+1)))))
    return res
print(reverse(765))


##  reverse of a number
# def rev(num):
#     res=0
#     n=num
#     while(n>0):
#         res=res*10+n%10
#         n=n//10
#     return res
# print(rev(653))


# lcm without using gcd
def multiples (num, stop):
    result = []
    for i in range(1, stop + 1):
        result.append(num * i)
    return result

def lcm (num1, num2):
    mul1 = multiples(num1, num2)
    mul2 = multiples(num2, num1)
    res=[]
    for i in mul1:
        if i in mul2:
            res.append(i)
    return min(res)
print(lcm(4,8))


# multiplying two numbers
def mul (x, y):
    res = x
    for i in range(1, y):
        res += x
    return res
print('Sample', mul('20', 5))

# index
def find(word, letter):
    index = 0
    while index < len(word):
        if word[index] == letter:
            return index
        index = index + 1
    return -1

# reverse
def is_reverse(word1, word2):
    if len(word1) != len(word2):
        return False
    i = 0
    j = len(word2)
    while j > 0:
        if word1[i] != word2[j-1]:
            return False
        i = i + 1
        j = j - 1
    return True
print(is_reverse('tanu','unat'))

# unique of a string
def  uniqe(word):
    res=''
    for i in word:
        if i not in res:
            res=res+i
    return res
print(uniqe("tannnuja"))
print(uniqe("helloworldhaaai"))



































